`import Ember from 'ember'`

DesignPackageComponent = Ember.Component.extend(
  packageSize: 'XSmall'
  boxesAmount: 0
  packageColor: 'White'

  XSmallInternal: true
  SmallInternal:  false
  MediumInternal: false
  LargeInternal:  false

  XSmall: (->
    @get('XSmallInternal')
  ).property('XSmallInternal', 'SmallInternal', 'MediumInternal', 'LargeInternal')

  Small: (->
    @get('SmallInternal')
  ).property('XSmallInternal', 'SmallInternal', 'MediumInternal', 'LargeInternal')

  Medium: (->
    @get('MediumInternal')
  ).property('XSmallInternal', 'SmallInternal', 'MediumInternal', 'LargeInternal')

  Large: (->
    @get('LargeInternal')
  ).property('XSmallInternal', 'SmallInternal', 'MediumInternal', 'LargeInternal')

  price: (->
    @get('boxesAmount') * 3
  ).property('boxesAmount')


  setSelectedFalse: ()->
    @set('XSmallInternal', false)
    @set('SmallInternal', false)
    @set('MediumInternal', false)
    @set('LargeInternal', false)


  actions:
    choosePackage: (packageSize)->
      @set('packageSize', packageSize)
      @setSelectedFalse()
      @set(packageSize + 'Internal', true)
)

`export default DesignPackageComponent`
