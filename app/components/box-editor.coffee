`import Ember from 'ember'`

BoxEditorComponent = Ember.Component.extend(
  side: 'Front'
  logUploaded: false

  choosenSide: (->
    @get('side')
  ).property('side')

  imageUrl: (->
    side = ''
    side = @get('side') if @get('color') == 'Green'

    'box-' + @get('color') + side + '.png'
  ).property('color', 'side')

  modelUrl: (->
    logo = ''
    logo = 'Logo' if @get('logoUploaded')

    'box3D-' + @get('color') + logo + '.png'
  ).property('color', 'logoUploaded')


  actions:
    chooseSide: ()->
      @set('side', event.target.dataset.side)

    upload: ()->
      @set('logoUploaded', true)
)

`export default BoxEditorComponent`
