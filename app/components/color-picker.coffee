`import Ember from 'ember'`

ColorPickerComponent = Ember.Component.extend(
  color: 'White'

  colorWhite: (->
    @get('color') == 'White'
  ).property('color')

  colorPurple: (->
    @get('color') == 'Purple'
  ).property('color')

  colorRed: (->
    @get('color') == 'Red'
  ).property('color')

  colorOrange: (->
    @get('color') == 'Orange'
  ).property('color')

  colorYellow: (->
    @get('color') == 'Yellow'
  ).property('color')

  colorGreen: (->
    @get('color') == 'Green'
  ).property('color')

  colorBlue: (->
    @get('color') == 'Blue'
  ).property('color')


  actions:
    selectColor: (color)->
      @set('color', color)
)

`export default ColorPickerComponent`
