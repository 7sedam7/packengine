`import Ember from 'ember'`

NumberInputComponent = Ember.Component.extend(

  keyUp: (event)->
    @set('value', event.target.value)
)

`export default NumberInputComponent`
