`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'box-editor', 'Integration | Component | box editor', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{box-editor}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#box-editor}}
      template block text
    {{/box-editor}}
  """

  assert.equal @$().text().trim(), 'template block text'
