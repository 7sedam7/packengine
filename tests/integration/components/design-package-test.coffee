`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'design-package', 'Integration | Component | design package', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{design-package}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#design-package}}
      template block text
    {{/design-package}}
  """

  assert.equal @$().text().trim(), 'template block text'
